import styled from 'styled-components';

const Wrapper = styled.footer`
  justify-content: space-between;
  padding: 3em 0;
  width: 100%;
  background-color: rgba(91, 251, 251, 0.3);
`;

export default Wrapper;
