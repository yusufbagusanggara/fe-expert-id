import React from 'react';
import styled from 'styled-components';
import Grid from '@material-ui/core/Grid';
import EmailIcon from '@material-ui/icons/Email';
import WhatsAppIcon from '@material-ui/icons/WhatsApp';
import InstagramIcon from '@material-ui/icons/Instagram';
import Wrapper from './Wrapper';
import H3 from '../H3';

const FooterStyle = styled.div`
  max-width: 1136px;
  margin: 0 auto;
`;

const KategoriKelasStyle = styled.div`
  text-align: left;
`;

const HubungiKamiStyle = styled(Grid)`
  text-align: right;
`;

function Footer() {
  return (
    <Wrapper>
      <FooterStyle>
        <Grid container>
          <Grid item xs={3}>
            <H3>Kategori Kelas</H3>
            <KategoriKelasStyle>Mechanical Design</KategoriKelasStyle>
            <KategoriKelasStyle>Architecture Design</KategoriKelasStyle>
            <KategoriKelasStyle>Rendering and Animation</KategoriKelasStyle>
            <KategoriKelasStyle>Rencana Anggaran Biaya</KategoriKelasStyle>
          </Grid>
          <Grid item xs={3}>
            <H3>Paket Eksklusif</H3>
            <KategoriKelasStyle>Kelas Tatap Muka</KategoriKelasStyle>
            <KategoriKelasStyle>Expert Class</KategoriKelasStyle>
            <KategoriKelasStyle>Intensive Class</KategoriKelasStyle>
          </Grid>
          <Grid item xs={3}>
            <H3>Menjadi Tentor</H3>
            <KategoriKelasStyle>Daftar Disini</KategoriKelasStyle>
          </Grid>
          <HubungiKamiStyle item xs={3}>
            <H3>Hubungi Kami</H3>
            <div>Jl. Ketintang, Kec Gayungan, Surabaya</div>
            <div>
              3dexpert.cad@gmail.com <EmailIcon />
            </div>
            <div>
              083857440487 <WhatsAppIcon />
            </div>
            <div>
              3d_expert.id <InstagramIcon />
            </div>
          </HubungiKamiStyle>
        </Grid>
      </FooterStyle>
    </Wrapper>
  );
}

export default Footer;
