import styled from 'styled-components';

import NormalImg from 'components/Img';

const Img = styled(NormalImg)`
  width: 100%;
  display: block;
  max-width: 200px;
`;

export default Img;
