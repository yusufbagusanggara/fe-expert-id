import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid';
import styled from 'styled-components';
import A from './A';
import Img from './Img';
import Logo from './logo.png';

const HeaderStyle = styled.div`
  background-color: #5bfbff;
  top: 0;
  position: fixed;
  width: 100%;
  z-index: 100;
`;

const GridContainerStyle = styled.div`
  margin: 0 auto;
  max-width: 1136px;
  display: flex;
  align-items: center;
`;

const GridItemStyle = styled(Grid)`
  text-align: center;
`;

function Header() {
  return (
    <HeaderStyle>
      <GridContainerStyle container>
        <GridItemStyle item xs={7}>
          <div>
            <A to="/">
              <Img src={Logo} alt="expert.id - Logo" />
            </A>
          </div>
        </GridItemStyle>
        <GridItemStyle item xs={1}>
          <A to="/">Home</A>
        </GridItemStyle>
        <GridItemStyle item xs={1}>
          <A to="/Kategori-Kelas">Kategori Kelas</A>
        </GridItemStyle>
        <GridItemStyle item xs={1}>
          Kelas Saya
        </GridItemStyle>
        <GridItemStyle item xs={1}>
          Transaksi
        </GridItemStyle>
        <Grid item xs={1} style={{ textAlign: 'right' }}>
          <div>
            <Avatar alt="Remy Sharp" src="/static/images/avatar/1.jpg" />
          </div>
        </Grid>
      </GridContainerStyle>
    </HeaderStyle>
  );
}

export default Header;
