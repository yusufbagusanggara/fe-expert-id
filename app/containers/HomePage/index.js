/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React, { memo } from 'react';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import { useInjectReducer } from 'utils/injectReducer';
import { useInjectSaga } from 'utils/injectSaga';
import {
  makeSelectRepos,
  makeSelectLoading,
  makeSelectError,
} from 'containers/App/selectors';
import Grid from '@material-ui/core/Grid';
import styled from 'styled-components';
import { Button, TextField, withStyles } from '@material-ui/core';
import SpaceHeader from '../../components/SpaceHeader';
import { loadRepos } from '../App/actions';
import { changeUsername } from './actions';
import { makeSelectUsername } from './selectors';
import reducer from './reducer';
import saga from './saga';
import Footer from '../../components/Footer';
import Logo from './logo.png';
import Img from './Img';
import Background from './background.jpg';

const key = 'home';

const ContainerHomeStyle = styled.div`
  width: 100%;
  background: url(${Background});
`;

const GridContainerHomeStyle = styled.div`
  max-width: 1136px;
  margin: 0 auto;
  margin-bottom: 100px;
`;

const ImgLogoStyle = styled.div`
  text-align: center;
`;

const H4 = styled.h4`
  color: #261fec;
`;

const H2 = styled.h2`
  color: #000000;
`;

const ColorButton = withStyles(() => ({
  root: {
    color: '#000000',
    backgroundColor: '#5bfbff',
    height: '56px',
  },
}))(Button);

const TextFieldCustom = withStyles(() => ({
  root: {
    width: '72%',
  },
}))(TextField);

export function HomePage() {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });

  return (
    <article>
      <Helmet>
        <title>Home Page</title>
        <meta name="Expert.id Home" content="Homepage" />
      </Helmet>
      <SpaceHeader />
      <ContainerHomeStyle>
        <GridContainerHomeStyle>
          <Grid container spacing={4}>
            <Grid item xs={6}>
              <ImgLogoStyle>
                <Img src={Logo} alt="logo home page" />
              </ImgLogoStyle>
              <H4>
                3D Expert.id merupakan sebuah platform kelas desain 3D online
                yang membantu kamu mendapat ilmu desain 3D dengan mudah dan
                flexibel.
              </H4>
            </Grid>
            <Grid item xs={6}>
              <div style={{ display: 'flex' }}>
                <div>
                  <TextField
                    id="outlined-basic"
                    label="Email"
                    variant="outlined"
                    helperText=""
                  />
                </div>
                <div style={{ marginLeft: '8px' }}>
                  <TextField
                    id="outlined-basic"
                    label="Kata Sandi"
                    variant="outlined"
                    helperText=""
                    type="password"
                  />
                </div>
                <div style={{ marginLeft: '8px' }}>
                  <ColorButton variant="contained" color="primary">
                    Login
                  </ColorButton>
                </div>
              </div>
              <H2>Daftar</H2>
              <div>
                <div style={{ display: 'flex', marginBottom: '8px' }}>
                  <div>
                    <TextField
                      id="outlined-basic"
                      label="Nama Depan"
                      variant="outlined"
                      helperText=""
                    />
                  </div>
                  <div style={{ marginLeft: '8px' }}>
                    <TextField
                      id="outlined-basic"
                      label="Nama Belakang"
                      variant="outlined"
                      helperText=""
                      type="password"
                    />
                  </div>
                </div>
                <div style={{ marginBottom: '8px' }}>
                  <TextFieldCustom
                    id="outlined-basic"
                    label="Email"
                    variant="outlined"
                    helperText=""
                  />
                </div>
                <div style={{ marginBottom: '8px' }}>
                  <TextFieldCustom
                    id="outlined-basic"
                    label="Kata Sandi"
                    variant="outlined"
                    helperText=""
                  />
                </div>
                <div style={{ marginBottom: '8px' }}>
                  <TextFieldCustom
                    id="outlined-basic"
                    label="Instansi / Universitas"
                    variant="outlined"
                    helperText=""
                  />
                </div>
              </div>
            </Grid>
          </Grid>
        </GridContainerHomeStyle>
        <Footer />
      </ContainerHomeStyle>
    </article>
  );
}

HomePage.propTypes = {
  // loading: PropTypes.bool,
  // error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
};

const mapStateToProps = createStructuredSelector({
  repos: makeSelectRepos(),
  username: makeSelectUsername(),
  loading: makeSelectLoading(),
  error: makeSelectError(),
});

export function mapDispatchToProps(dispatch) {
  return {
    onChangeUsername: evt => dispatch(changeUsername(evt.target.value)),
    onSubmitForm: evt => {
      if (evt !== undefined && evt.preventDefault) evt.preventDefault();
      dispatch(loadRepos());
    },
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(HomePage);
